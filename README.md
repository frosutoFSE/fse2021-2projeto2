# FSE 2021.2 - Projeto 2

## Identificação

**Nome**: Wagner Martins da Cunha

**Matrícula**: 18/0029177

## Sobre

Repositório de desenvolvimento do [trabalho 2](https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-2-2021-2) de Fundamentos de Sistemas Embarcados.

O programa simula um sistema de controle predial, composto por um servidor central e servidores distribuidos. Cada servidor distribuído interfaceia os dispositivos de um andar.

## Servidor Central

O servidor central foi criado utilizando a linguagem python (mais especificamente a versão 3.7, instalada na rasp43), e utiliza as seguintes bibliotecas(módulos):

- socket
- threading
- json
- queue
- curses
- sys

Todos estes módulos são esperados já virem instalados na standard library do python 3, e por isso não é necessário nenhuma instalação a mais.

### Rodando Servidor Central

Na `rasp43`, navegue até a pasta `central_server`, e utilize o comando:

```shell
$ python3 main.py
```

para iniciar a aplicação. A aplicação lerá o arquivo `server_config.json` para configurar o servidor, e começará a escutar por novas conexões. Ao mesmo tempo o terminal ficará no modo curses. Assim que a primeira conexão for estabelecida, os dados enviados pelo servidor distribuido serão mostrados em tela.

Para exibir os comandos da aplicação, aperte a tecla `h`. Abaixo, a lista completa de comandos disponíveis:

- `h`: entra e sai da tela de comandos
- `q`: sai da aplicacao
- `direcionais para dos lados`: troca entre andares
- `direcionais para cima e baixo`: move o cursor*
- `Enter`: liga/desliga aparelho selecionado
(só funciona onde aparece o cursor '>')
- `l`/`d`: desliga/liga todo o predio
- `i`/`o`: desliga/liga todo o andar atualmente selecionado
- \* O cursor é um dos caracteres '>' ou '-'
    - '`>`' significa um dispositivo de saida e pode ser ativado/desativado
    - '`-`' significa um dispositivo de entrada, e não tem interação

São salvos dois arquivos log. Um chamado `error_log.csv`, que contém a mensagem de erro de exceções capturadas. E o `log.csv` que contém as mensagens de interação do usuário, ativação ou desligamento do alarme de incêndio e inicialização e término do servidor. Dentre os comandos do usuário, apenas comandos de ligar/desligar os dispositivos são salvos no log, logo, as ações de mover o cursor, trocar a visão entre os andares e exibir a tela de comandos não geram registros no log.

## Servidor Distribuído

Na `rasp42`, navegue até a pasta `distributed_server`. O código foi implementado em C++. Para compilar, utilize o comando:

```shell
$ make
```

Para rodar o programa utilizando o make, é necessário criar uma variável de ambiente com o caminho para o arquivo de configuração. Neste caso, recomenda-se usar o comando:

```shell
$ make file=<arquivo_de_configuracao> run
```

Os dois arquivos disponíveis atualmente são:
`configuracao_andar_terreo.json` e `configuracao_andar_1.json`

Caso se deseje executar o programa sem o make:

```shell
$ ./bin/main <arquivo_de_configuracao>
```

Para apagar todos os arquivos de objeto e binários gerados, utilize o comando:

```shell
$ make clean
```

**`Obs.:`** Foram feitas algumas modificações pontuais nos arquivos de configuração, sendo elas:

- Mudança das portas, de acordo com o mapeamento definido pelo professor.
- Mudança do endereço ip dos servidores distribuídos. O endereço passado não estava funcionando.
- Os sensores de contagem tiveram o nome de seus tipos alterados de 'contagem' para 'contagem_entrada' e 'contagem_saida' para facilitar a identificação de suas respectivas responsabilidades.
- campo tipo nos sensores de temperatura alterado de 'dth22' para 'dht22'.

### Problemas identificados no código implementado

O requisito de alarme com presença não foi implementado.

Todos os servidores iniciam normalmente. O servidor central espera por novas conexões e os distribuídos ficam esporadicamente tentando conectar com o servidor central, ao mesmo tempo que fazem as leituras dos dispositivos no andar. Entretanto o sistema não está estável ao desconectar qualquer um deles. O servidor central manterá o estado do andar mesmo após desconectado e o servidor distribuído volta a tentar realizar uma nova conexão, que por sua vez nunca tem sucesso.

