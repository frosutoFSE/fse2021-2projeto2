#include <unordered_map>
#include <utility>
#include <string>
#include "../inc/devices.hpp"
#include "../inc/bcm_helper_constants.hpp"
#include "../inc/bcm_wrapper.hpp"
#include "../inc/cJSON.h"

namespace OutputOnOff {

static std::unordered_map<int8_t, std::pair<Device, int8_t>> devices;

void registerDevice(Device dev) {
    devices[dev.pin] = std::pair<Device, int8_t>(dev, OFF);
    bcm::setupPin(dev.pin, OUTPUT, PULLDOWN);
}

void setDeviceState(int8_t pin, OnOff state) {
    bcm::setState(pin, state);
    devices[pin].second = state;
}

void getDeviceList(cJSON* array) {
    for (auto i : devices) {
        cJSON* obj = serializeDevice(i.second.first);
        cJSON_AddItemToArray(array, obj);
    }
}

cJSON* serializeData(int8_t pin) {
    cJSON* obj = serializeDevice(devices[pin].first);
    cJSON_AddStringToObject(obj, "type", devices[pin].first.type.c_str());
    cJSON_AddNumberToObject(obj, "data", devices[pin].second);
    return obj;
}

}
