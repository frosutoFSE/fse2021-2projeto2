#include <cstdio>
#include <cstring>
#include <cstdint>
#include <utility>
#include <unistd.h>

#include <pthread.h>

extern "C" {
#include "../inc/dht22.h"
}

// holds temperature and humidity
static std::pair<float, float> thData {0, 0};
static pthread_t thread;
static pthread_mutex_t mtx;
static bool stop_flag = false;

void updateData(std::pair<float, float> data) {
    pthread_mutex_lock(&mtx);
    thData = data;
    pthread_mutex_unlock(&mtx);
}

void* dht22_readLoop(void*) {
    float temperature, humidity;
    while (true) {
        if (!dht22_read_data(&temperature, &humidity)) {
            updateData(std::pair<float, float>(temperature, humidity));
        }
        sleep(1);
        if (stop_flag) {
            pthread_exit(0);
        }
    }
}

namespace dht22 {

int setup(int pin) {
    int rslt = dht22_init(pin);
    if (rslt) {
        return rslt;
    }

    pthread_create(&thread, NULL, dht22_readLoop, NULL);
    return 0;
}

std::pair<float, float> getTemperatureAndHumidity() {
    std::pair<float, float> temp;
    pthread_mutex_lock(&mtx);
    temp = thData;
    pthread_mutex_unlock(&mtx);
    return temp;
}

void close() {
    stop_flag = true;
    pthread_join(thread, NULL);
}

}
