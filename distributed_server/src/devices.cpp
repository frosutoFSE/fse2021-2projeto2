#include <tuple>
#include <string>
#include "../inc/devices.hpp"

Device* createDevice(int8_t pin, std::string tag, std::string type, int gSleep, int lSleep) {
    Device *dev = new Device();
    dev->pin = pin;
    dev->tag = tag;
    dev->type = type;
    dev->count = 0;
    dev->gsleep = gSleep;
    dev->lsleep = lSleep;
    return dev;
}

bool canReadData(Device* dev) {
    int rslt = dev->gsleep / dev->lsleep;
    dev->count += 1;
    if (dev->count >= rslt) {
        dev->count = 0;
        return true;
    }
    return false;
}

template<typename T>
T getData(Device dev, std::function<T(std::string)> convert) {
    return convert(dev.data);
}

template<typename T>
void setData(Device *dev, T newData, std::function<std::string(T)> convert) {
    dev->data = convert(newData);
}
