#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>
#include <error.h>

#include "../inc/tcp.hpp"

#define MESSAGE_MAX_SIZE 2048

namespace tcp {

int tcpSock = 0;
struct sockaddr_in serverAddr;
std::string ipServer;
unsigned short portServer;

bool connected;

int bind(std::string selfIp, unsigned short selfPort) {

    tcpSock = socket(PF_INET, SOCK_STREAM, 0);
    if (tcpSock < 0) {
        printf("Erro ao abrir o socket\n");
        return -1;
    }

    struct sockaddr_in selfAddr;
    memset(&selfAddr, 0, sizeof(selfAddr));
    selfAddr.sin_family = AF_INET;
    selfAddr.sin_addr.s_addr = inet_addr(selfIp.c_str());
    selfAddr.sin_port = htons(selfPort);

    int bindRslt = ::bind(tcpSock, (struct sockaddr*)&selfAddr, sizeof(selfAddr));
    if (bindRslt < 0) {
        printf("Erro ao dar bind no socket\n");
        printf("%d\n", errno);
        return -1;
    }

    return 0;
}

int setup(std::string ipServer_, unsigned short portServer_) {
    ipServer = ipServer_;
    portServer = portServer_;

    struct sockaddr_in serverAddr;
    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = inet_addr(ipServer.c_str());
    serverAddr.sin_port = htons(portServer);

    int connRes = connect(tcpSock, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
    if (connRes < 0) {
//         printf("Erro ao conectar ao server\n");
        return -2;
    }

    connected = true;
    return 0;
}

int send(std::string message) {
    unsigned int rslt = ::send(tcpSock, message.c_str(), message.length(), 0);
    if (rslt != message.length()) {
        if (errno == ECONNRESET) {
            connected = false;
        }
        return -1;
    }
    return 0;
}

int recv(std::string& buffer) {
    char message[MESSAGE_MAX_SIZE];
    memset(message, 0, MESSAGE_MAX_SIZE);
//     ssize_t rslt = ::recv(tcpSock, message, MESSAGE_MAX_SIZE, MSG_WAITALL);
    ssize_t rslt = read(tcpSock, message, MESSAGE_MAX_SIZE);
    if (rslt <= 0) {
        connected = false;
        return rslt;
    }
//     printf("Mensagem Recebida:\n%s\n", message);
    buffer = std::string(message);
    return rslt;
}

void close() {
    ::close(tcpSock);
    tcpSock = 0;
}

bool isSocketConnected() {
    return connected;
}

}

