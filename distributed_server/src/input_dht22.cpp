#include <unordered_map>
#include <vector>
#include <utility>
#include <string>
#include "../inc/devices.hpp"
#include "../inc/dht22_wrapper.hpp"
#include "../inc/cJSON.h"

namespace InputDHT {

bool running = false;
Device dev;
float temperatura = 0;
float umidade = 0;

void registerDevice(Device dev_) {
    dev = dev_;

    if (!dht22::setup(dev.pin)) {
        running = true;
    }
}

bool stateChange() {
    bool change = false;
    std::pair<float, float> th = dht22::getTemperatureAndHumidity();
    if (th.first != temperatura) {
        temperatura = th.first;
        change = true;
    }
    if (th.second != umidade) {
        umidade = th.second;
        change = true;
    }
    return change;
}

std::pair<float, float> getData() {
    return std::pair<float, float>(temperatura, umidade);
}

cJSON* getContext() {
    cJSON* obj = cJSON_CreateObject();
    cJSON_AddStringToObject(obj, "tag", "Temperatura e umidade");
    cJSON_AddStringToObject(obj, "type", "dht22");
    return obj;
}

cJSON* serializeData() {
    cJSON* obj = cJSON_CreateObject();
    cJSON_AddStringToObject(obj, "type", "dht22");
    cJSON_AddNumberToObject(obj, "temperatura", temperatura);
    cJSON_AddNumberToObject(obj, "umidade", umidade);
    return obj;
}

// cJSON* getDeviceList() {
//     cJSON* array = cJSON_CreateArray();
//
//     cJSON* obj = serializeDevice(dev);
//     cJSON_AddFalseToObject(obj, "centralctrl");
//     cJSON_AddItemToArray(array, obj);
//
//     return array;
// }

}
