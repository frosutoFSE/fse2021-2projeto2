#include <cstdio>
#include <string>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <iostream>

#include "../inc/controller.hpp"
#include "../inc/bcm_wrapper.hpp"
#include "../inc/dht22_wrapper.hpp"
#include "../inc/cJSON.h"
#include "../inc/devices.hpp"
#include "../inc/output_onoff.hpp"
#include "../inc/input_onoff.hpp"
#include "../inc/input_counter.hpp"
#include "../inc/input_dht22.hpp"
#include "../inc/tcp.hpp"

std::string selfIp;
int selfPort;

std::string centralIp;
int centralPort;

std::string selfName;

void configure(cJSON* content) {
    centralIp = cJSON_GetObjectItem(content, "ip_servidor_central")->valuestring;
    centralPort = cJSON_GetObjectItem(content, "porta_servidor_central")->valueint;
    selfIp = cJSON_GetObjectItem(content, "ip_servidor_distribuido")->valuestring;
    selfPort = cJSON_GetObjectItem(content, "porta_servidor_distribuido")->valueint;
    selfName = cJSON_GetObjectItem(content, "nome")->valuestring;
}

int readCommandLine(int argc, char** argv) {
    if (argc < 2) {
        printf("Uso: %s <caminho_arquivo_de_configuracao>\n", argv[0]);
        return 1;
    }
    return 0;
}

std::string readFile(const char* path, int* error) {
    std::ifstream inFile;
    inFile.open(path);

    if (not inFile.is_open()) {
        *error = 1;
        return "";
    }

    std::stringstream strStream;
    strStream << inFile.rdbuf();
    return strStream.str();
}

void inputControl() {
//     read each device
//     update state and send to controller queue
    int numOnOff = InputOnOff::getNumberOfDevices();

    while (true) {

//      lê os devices onoff
        for (int i = 0; i < numOnOff; i++) {
            if (InputOnOff::stateChange(i)) {
                cJSON* data = InputOnOff::serializeData(i);
                Ctrl::enqueueMessage(cJSON_Print(data));
            }
        }
//      lê o contador
        if (InputCounter::stateChange()) {
            cJSON* data = InputCounter::serializeData();
            Ctrl::enqueueMessage(cJSON_Print(data));
        }
//      lê temperatura e humidade
        if (InputDHT::stateChange()) {
            cJSON* data = InputDHT::serializeData();
            Ctrl::enqueueMessage(cJSON_Print(data));
        }

        usleep(50000);
    }
}

int main (int argc, char** argv) {

    if (readCommandLine(argc, argv)) return 1;
    printf("%s\n", argv[1]);
//     Ctrl::init(config.getIpCentral(), config.getPortCentral());

    int error = 0;
    std::string file_content = readFile(argv[1], &error);
    if (error) {
        printf("Não foi possível ler o arquivo\n");
        return 1;
    }

    if (bcm::init()) {
        printf("Falha ao iniciar biblioteca bcm2835\n");
        return 1;
    }

//     seta variáveis globais
    cJSON* config = cJSON_Parse(file_content.c_str());
    configure(config);

    if (tcp::bind(selfIp, selfPort)) {
        return 1;
    }

// registra output devices
    cJSON* dev;
    cJSON* outputDevs = cJSON_GetObjectItem(config, "outputs");
    cJSON_ArrayForEach(dev, outputDevs) {
        int8_t pin = cJSON_GetObjectItem(dev, "gpio")->valueint;
        std::string tag = cJSON_GetObjectItem(dev, "tag")->valuestring;
        std::string type = cJSON_GetObjectItem(dev, "type")->valuestring;
        OutputOnOff::registerDevice(
            *createDevice(pin, tag, type, 0, 0)
        );
    }

// registra input devices
    cJSON* inputOnOff = cJSON_GetObjectItem(config, "inputs");
    cJSON_ArrayForEach(dev, inputOnOff) {
        int8_t pin = cJSON_GetObjectItem(dev, "gpio")->valueint;
        std::string tag = cJSON_GetObjectItem(dev, "tag")->valuestring;
        std::string type = cJSON_GetObjectItem(dev, "type")->valuestring;
        if (type == "contagem_entrada") {
            InputCounter::registerDevice(
                *createDevice(pin, tag, type, 0, 0),
                "contagem_entrada"
            );
        }
        else if (type == "contagem_saida") {
            InputCounter::registerDevice(
                *createDevice(pin, tag, type, 0, 0),
                "contagem_saida"
            );
        }
        else {
            InputOnOff::registerDevice(
                *createDevice(pin, tag, type, 0, 0)
            );
        }
    }

// registra sensor de temperatura e umidade
    cJSON* sensor = cJSON_GetObjectItem(config, "sensor_temperatura");
    cJSON_ArrayForEach(dev, sensor) {
        int8_t pin = cJSON_GetObjectItem(dev, "gpio")->valueint;
        std::string tag = cJSON_GetObjectItem(dev, "tag")->valuestring;
        std::string type = cJSON_GetObjectItem(dev, "type")->valuestring;
        InputDHT::registerDevice(
            *createDevice(pin, tag, type, 0, 0)
        );
    }

// envia dados deste servidor para o central
    cJSON* selfDataArray = cJSON_CreateArray();
    OutputOnOff::getDeviceList(selfDataArray);
    InputOnOff::getDeviceList(selfDataArray);
    cJSON* counter = InputCounter::getContext();
    cJSON_AddItemToArray(selfDataArray, counter);
    cJSON* sensorT = InputDHT::getContext();
    cJSON_AddItemToArray(selfDataArray, sensorT);

    cJSON* selfDataObj = cJSON_CreateObject();
    cJSON_AddStringToObject(selfDataObj, "name", selfName.c_str());
    cJSON_AddItemToObject(selfDataObj, "data", selfDataArray);

    std::string selfData = cJSON_Print(selfDataObj);

    Ctrl::init(centralIp, centralPort, selfData);

    inputControl();

    return 0;

}
