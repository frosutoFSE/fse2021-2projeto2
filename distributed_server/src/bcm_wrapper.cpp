#include <cstdio>
#include <cstdlib>
#include <bcm2835.h>
#include <unistd.h>
#include <functional>

#include "../inc/bcm_helper_constants.hpp"
#include "../inc/bcm_wrapper.hpp"

namespace bcm {

int init() {
    if (!bcm2835_init()) return -1;
    return 0;
}

void close() {
    bcm2835_close();
}

// void setupPin(int pin, Mode m, PullState ps, int eventFlag) {
//     bcm2835_gpio_fsel(pin, m);
//     bcm2835_gpio_set_pud(pin, ps);
//
//     if (eventFlag & E_HIGH)        bcm2835_gpio_hen(pin);
//     if (eventFlag & E_LOW)         bcm2835_gpio_len(pin);
//     if (eventFlag & E_RISINGEDGE)  bcm2835_gpio_ren(pin);
//     if (eventFlag & E_FALLINGEDGE) bcm2835_gpio_fen(pin);
// }

void setupPin(int pin, Mode m, PullState ps) {
    bcm2835_gpio_fsel(pin, m);
    if (ps != NOPULL)
        bcm2835_gpio_set_pud(pin, ps);
}

void setState(int pin, OnOff st) {
    bcm2835_gpio_write(pin, st);
}

int getState(int pin) {
    return bcm2835_gpio_lev(pin);
}

}
// int main() {
//
//     if (!bcm2835_init()) exit(1);
//
//     bcm2835_gpio_fsel(LAMP, BCM2835_GPIO_FSEL_OUTP);
//
//     bcm2835_gpio_set_pud(LAMP, BCM2835_GPIO_PUD_DOWN);
//
//     bcm2835_gpio_write(LAMP, LOW);
//
//     sleep(10);
//
//     bcm2835_gpio_write(LAMP, LOW);
//
//     bcm2835_close();
// }
