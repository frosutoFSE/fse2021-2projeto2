#include <unordered_map>
#include <vector>
#include <utility>
#include <string>
#include "../inc/devices.hpp"
#include "../inc/bcm_helper_constants.hpp"
#include "../inc/bcm_wrapper.hpp"
#include "../inc/cJSON.h"

namespace InputCounter {

std::pair<std::pair<Device, Device>, int> counter;
int inState = OFF;
int outState = OFF;

void registerDevice(Device dev, std::string type) {
    counter.second = 0;
    if (type == "contagem_entrada") {
        counter.first.first = dev;
        bcm::setupPin(dev.pin, INPUT, NOPULL);
    }

    if (type == "contagem_saida") {
        counter.first.second = dev;
        bcm::setupPin(dev.pin, INPUT, NOPULL);
    }
}

bool inStateChange() {
    int newInState = bcm::getState(counter.first.first.pin);
    if (newInState == ON && inState == OFF) {
         counter.second += 1;
         inState = newInState;
         return true;
    }
    else if (newInState == OFF && inState == ON) {
        inState = newInState;
    }
    return false;
}

bool outStateChange() {
    int newOutState = bcm::getState(counter.first.second.pin);
    if (newOutState == ON && outState == OFF) {
         counter.second = counter.second > 0 ? counter.second - 1 : counter.second;
         outState = newOutState;
         return true;
    }
    else if (newOutState == OFF && outState == ON) {
        outState = newOutState;
    }
    return false;
}

bool stateChange() {
    return inStateChange() || outStateChange();
}

int getCounter() {
    return counter.second;
}

cJSON* getContext() {
    cJSON* obj = cJSON_CreateObject();
    cJSON_AddStringToObject(obj, "tag", "contador de pessoas no predio");
    cJSON_AddStringToObject(obj, "type", "counter");
    return obj;
}

cJSON* serializeData() {
    cJSON* obj = cJSON_CreateObject();
    cJSON_AddStringToObject(obj, "type", "counter");
    cJSON_AddNumberToObject(obj, "data", counter.second);
    return obj;
}

}
