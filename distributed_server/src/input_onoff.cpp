#include <unordered_map>
#include <vector>
#include <utility>
#include <string>
#include "../inc/devices.hpp"
#include "../inc/bcm_helper_constants.hpp"
#include "../inc/bcm_wrapper.hpp"
#include "../inc/cJSON.h"

namespace InputOnOff {

std::vector<std::pair<Device, int8_t>> devices;

void registerDevice(Device dev) {
    devices.emplace_back(std::pair<Device, int8_t>(dev, OFF));
    bcm::setupPin(dev.pin, INPUT, NOPULL);
}

int getNumberOfDevices() {
    return devices.size();
}

bool stateChange(int i) {
    int new_state = bcm::getState(devices[i].first.pin);
    if (new_state == devices[i].second) {
        return false;
    }
    devices[i].second = new_state;
    return true;
}

int8_t getState(int i) {
    return devices[i].second;
}

void getDeviceList(cJSON* array) {
    for (auto i : devices) {
        cJSON* obj = serializeDevice(i.first);
        cJSON_AddItemToArray(array, obj);
    }
}

cJSON* serializeData(int i) {
    cJSON* obj = serializeDevice(devices[i].first);
    cJSON_AddStringToObject(obj, "type", devices[i].first.type.c_str());
    cJSON_AddNumberToObject(obj, "data", devices[i].second);
    return obj;
}

}
