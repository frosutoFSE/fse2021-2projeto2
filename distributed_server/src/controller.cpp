#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <queue>
#include <mutex>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#include "../inc/tcp.hpp"
#include "../inc/controller.hpp"

#include "../inc/output_onoff.hpp"
#include "../inc/input_onoff.hpp"
#include "../inc/input_counter.hpp"
#include "../inc/input_dht22.hpp"

#define MAX_TRY 5
#define QUEUE_MAX_SIZE 100

static pthread_t threadIN;
static pthread_t threadOUT;

static std::queue<std::string> qSendBuffer;
static pthread_mutex_t queueMut;

static bool stopThreads = false;

static std::string serverIp;
static unsigned short serverPort;

static std::string selfData;

namespace Ctrl {

void exitThread() {
    pthread_exit(0);
}

void enqueueMessage(std::string message) {

    if (!tcp::isSocketConnected()) {
        return;
    }

    pthread_mutex_lock(&queueMut);
    qSendBuffer.push(message);
    pthread_mutex_unlock(&queueMut);
}

std::string popMessage() {
    while (true) {
        if (stopThreads) {
            exitThread();
        }
        if (qSendBuffer.empty()) {
            usleep(10000);
            continue;
        }
        pthread_mutex_lock(&queueMut);
        std::string temp = qSendBuffer.front();
        qSendBuffer.pop();
        pthread_mutex_unlock(&queueMut);
        return temp;
    }
}

void* receiveCommands(void*) {
    pthread_detach(pthread_self());
    std::string buffer;
    while (true) {
        if (not tcp::isSocketConnected() || stopThreads) {
            exitThread();
        }
        if (tcp::recv(buffer) <= 0) {
            continue;
        }

        cJSON* data = cJSON_Parse(buffer.c_str());
        int pin = cJSON_GetObjectItem(data, "pin")->valueint;
        int state = cJSON_GetObjectItem(data, "data")->valueint;
        OutputOnOff::setDeviceState(pin, (OnOff)state);
        cJSON_Delete(data);

        cJSON* sdata = OutputOnOff::serializeData(pin);
        enqueueMessage(cJSON_Print(sdata));
    }
}

void* sendCommands(void*) {
    pthread_detach(pthread_self());
    while (true) {
        std::string msg = popMessage();
        if (not tcp::isSocketConnected() || stopThreads) {
            exitThread();
        }
        if (tcp::send(msg) < 0) {
            continue;
        }
    }
}

void tryConnect() {
    printf("Tentando conectar ao servidor central\n");
    while (true) {
        if (stopThreads) {
            exitThread();
        }
        if (tcp::setup(serverIp, serverPort) == 0) {
            printf("Conectado ao servidor central\n");
            return;
        }
        sleep(1);
    }
}

void* connectionLoop(void*) {
//     int rslt;
    while (not stopThreads) {
        if (not tcp::isSocketConnected()) {
            tryConnect();
            pthread_t recv, send;
            tcp::send(selfData);
            pthread_create(&recv, NULL, receiveCommands, 0);
            pthread_create(&send, NULL, sendCommands, 0);
        }
        usleep(500000);
    }
    exitThread();
    return NULL;
}

int init(std::string ipServer, unsigned short portServer, std::string selfData_) {

    serverIp = ipServer;
    serverPort = portServer;
    selfData = selfData_;

    if (pthread_mutex_init(&queueMut, NULL) != 0) {
        printf("Erro ao iniciar mutex\n");
        return -1;
    }

    pthread_t connLoop;
    pthread_create(&connLoop, NULL, connectionLoop, 0);

    qSendBuffer = std::queue<std::string>();

    return 0;
}

void close() {
    stopThreads = true;
    pthread_join(threadIN, NULL);
    enqueueMessage(std::string("encerrar"));
    pthread_join(threadOUT, NULL);
    tcp::close();
}

bool mustStopController() {
    return stopThreads;
}

}
