#ifndef _INPUT_COUNTER_HPP_
#define _INPUT_COUNTER_HPP_

#include "devices.hpp"

namespace InputCounter {

void registerDevice(Device d, std::string type);
bool stateChange();
int getCounter();
cJSON* getContext();
cJSON* serializeData();

}

#endif
