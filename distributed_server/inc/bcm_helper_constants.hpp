#ifndef _BCM_HELPER_CONSTANTS_
#define _BCM_HELPER_CONSTANTS_

#include <bcm2835.h>

#define E_HIGH         1
#define E_LOW          2
#define E_RISINGEDGE   4
#define E_FALLINGEDGE  8

enum Mode {
    OUTPUT = BCM2835_GPIO_FSEL_OUTP,
    INPUT = BCM2835_GPIO_FSEL_INPT
};

enum PullState {
    PULLDOWN = BCM2835_GPIO_PUD_DOWN,
    PULLUP = BCM2835_GPIO_PUD_UP,
    NOPULL
};

enum OnOff {
    ON = HIGH, OFF = LOW
};

#endif
