/*
 *  Header adaptado a partir do driver do dht22, por Juergen Wolf-Hofer.
 */

#ifndef _DHT22_H_
#define _DHT22_H_

#include <stdint.h>

int dht22_init(int8_t pin);
int dht22_read_data(float* temperature, float* humidity);

#endif
