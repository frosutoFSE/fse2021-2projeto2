#ifndef _DHT22_WRAPPER_HPP_
#define _DHT22_WRAPPER_HPP_

#include <utility>

namespace dht22 {
    int setup(int pin);
    std::pair<float, float> getTemperatureAndHumidity();
    void close();
}

#endif
