#ifndef _OUTPUT_ONOFF_HPP_
#define _OUTPUT_ONOFF_HPP_

#include "devices.hpp"
#include "bcm_helper_constants.hpp"

namespace OutputOnOff {

void registerDevice(Device dev);
void setDeviceState(int8_t pin, OnOff state);
void getDeviceList(cJSON* array);
cJSON* serializeData(int8_t pin);

}

#endif
