#ifndef _DEVICES_HPP_
#define _DEVICES_HPP_

#include <cstdio>
#include <unordered_map>
#include <tuple>
#include <string>
#include <functional>
#include "cJSON.h"

class Device {
public:
    int8_t pin;
    std::string tag;
    std::string type;
    int count;
    int gsleep;
    int lsleep;
    std::string data;
};

Device* createDevice(int8_t pin, std::string tag, std::string type, int sleepTime, int waitTime);
bool canReadData(Device dev);

template<typename T>
T getData(Device dev, std::function<T(std::string)> convert);

template<typename T>
void setData(Device *dev, T newData, std::function<std::string(T)> convert);

inline int getlsleepFromType(std::string type) {
    if (type == "dht22") {
        return 1000;
    }
    return 0;
}

inline cJSON* serializeDevice(Device dev) {
    cJSON* json = cJSON_CreateObject();
    cJSON_AddNumberToObject(json, "pin", dev.pin);
    cJSON_AddStringToObject(json, "tag", dev.tag.c_str());
    cJSON_AddStringToObject(json, "type", dev.type.c_str());
    return json;
}

#endif
