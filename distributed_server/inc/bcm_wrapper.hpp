#ifndef _BCM_WRAPPER_HPP_
#define _BCM_WRAPPER_HPP_

#include "bcm_helper_constants.hpp"

namespace bcm {

int init();
void close();
void setupPin(int pin, Mode m, PullState ps);
void setState(int pin, OnOff);
int getState(int pin);

}
#endif
