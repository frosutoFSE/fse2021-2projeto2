#ifndef _TCP_HPP_
#define _TCP_HPP_

#include <string>

namespace tcp {

int bind(std::string ip, unsigned short port);
int setup(std::string ipServer_, unsigned short portServer_);
int send(std::string message);
int recv(std::string& buffer);
void close();
bool isSocketConnected();

}

#endif
