#ifndef _CONTROLLER_HPP_
#define _CONTROLLER_HPP_

#include <string>

namespace Ctrl {

void enqueueMessage(std::string message);
std::string popMessage();
int init(std::string ipServer, unsigned short portServer, std::string selfData_);
void close();
bool mustStopController();

}

#endif
