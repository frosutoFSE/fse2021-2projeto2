#ifndef _INPUT_DHT22_HPP_
#define _INPUT_DHT22_HPP_

#include "devices.hpp"

namespace InputDHT {

void registerDevice(Device dev_);
bool stateChange();
std::pair<float, float> getData();
cJSON* getContext();
cJSON* serializeData();

}

#endif
