#ifndef _INPUT_ONOFF_HPP_
#define _INPUT_ONOFF_HPP_

#include "../inc/devices.hpp"

namespace InputOnOff {

void registerDevice(Device dev);
int getNumberOfDevices();
bool stateChange(int i);
int8_t getState(int i);
void getDeviceList(cJSON* array);
cJSON* serializeData(int i);

}


#endif
