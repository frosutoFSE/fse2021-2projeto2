import curses
import json
from typing import Dict, Optional, List, Union, Callable

from message_queue import MessageQueue

class Device:
    """
    mantém dados dos dispositivos
    """
    pin: Optional[int]
    tag: str
    type_: str
    state: int

    def __init__(self, pin, tag, type_):
        self.pin = pin
        self.tag = tag
        self.type_ = type_
        self.state = 0

    # def register_handler(self, func: Callable):
    #     self.handler = func

class Floor:
    """
    Mantém os dados de um andar
    """
    outDevices: Dict[str, Device]
    inDevices: Dict[str, Device]
    counter: int
    temperature: float
    humidity: float
    name: str
    mq: MessageQueue
    win: 'curses._CursesWindow'

    def __init__(self, mq: MessageQueue):
        self.outDevices = dict()
        self.inDevices = dict()
        self.counter = 0
        self.temperature = 0
        self.humidity = 0
        self.name = ''
        self.win = None
        self.mq = mq

    def __repr__(self) -> str:
        return f"""
        name: {self.name}
        counter: {self.counter}
        temperature: {self.temperature}
        humidity: {self.humidity}
        outDevices: {self.outDevices}
        inDevices: {self.inDevices}
        """

    def set_win(self, win: 'curses._CursesWindow'):
        self.win = win

    def print_data(self) -> List[str]:

        # if refresh:
        # self.win.clear()

        printer = Printer(self.win, 0, 0)

        def on_off(state: int) -> str:
            return 'ON' if state == 1 else 'OFF'

        printer.writeln((self.name, 'na', 0))
        printer.writeln((f'Número de pessoas: {self.counter}', 'in', 0))
        printer.writeln(('Temperatura: {:.2f}'.format(self.temperature), 'in', 0))
        printer.writeln(('Umidade: {:.2f}'.format(self.humidity), 'in', 0))
        for dev in self.outDevices:
            text = self.outDevices[dev].tag + ' ' + on_off(self.outDevices[dev].state)
            printer.writeln((text, 'out', self.outDevices[dev].pin))

        for dev in self.inDevices:
            text = self.inDevices[dev].tag + ' ' + on_off(self.inDevices[dev].state)
            printer.writeln((text, 'in', 0))
        
        # if refresh == True:
        # self.win.refresh()
        return printer.get_list()


    def set_name(self, name):
        self.name = name

    def register_device(self, dev_data: Dict):
        if dev_data.get('type') in ['contagem_entrada', 'contagem_saida', 'counter']:
            return

        if dev_data.get('type') in ['dht22']:
            return

        if dev_data.get('type') in ['lampada', 'ar-condicionado', 'aspersor']:
            # registra output devices
            pin = dev_data.get('pin', None)
            tag = dev_data.get('tag')
            type_ = dev_data.get('type')
            self.outDevices[pin] = Device(pin, tag, type_)
            return

        pin = dev_data.get('pin', None)
        tag = dev_data.get('tag')
        type_ = dev_data.get('type')
        self.inDevices[pin] = Device(pin, tag, type_)

    def set_state(self, pin: int, io: str, state: int):
        if io == "i":
            self.inDevices[pin].state = state
        if io == "o":
            self.outDevices[pin].state = state

    # def process_msg(self, data: Dict):
    #     pin = data.get('pin')
    #     _type = data.get('type')
    #     if _type is None:
    #         return

    #     if _type == 'counter':
    #         self.counter = data.get('data', self.counter)
    #         return

    #     if _type == 'dht22':
    #         self.temperature = data.get('temperatura', self.temperature)
    #         self.humidity = data.get('umidade', self.humidity)
    #         return

    #     if _type in ['lampada', 'ar-condicionado', 'aspersor']:
    #         state = data.get('data', self.outDevices[pin].state)
    #         self.outDevices[pin].state = state
    #         return

    #     state = data.get('data', self.inDevices[pin].state)
    #     self.inDevices[pin].state = state
    #     return

class Printer:
    win: 'curses._CursesWindow'
    y: int
    x: int

    def __init__(self, win, y = 0, x = 0):
        self.win = win
        self.y = y
        self.x = x
        self.list_text = []

    def writeln(self, text):
        self.list_text.append(text)
    
    def get_list(self):
        return self.list_text
