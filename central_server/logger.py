import csv
import datetime
from threading import Lock

class Logger():

    def __init__(self, file_name):
        self.file = file_name
        self.lock = Lock()

    def log(self, msg: str):
        self.lock.acquire()
        with open(self.file, 'a') as fd:
            data = [datetime.datetime.now().strftime("%d/%m/%Y"), datetime.datetime.now().strftime("%H:%M:%S"), msg]
            writer = csv.writer(fd)
            writer.writerow(data)
        self.lock.release()

    def log_header(self, data_name: str):
        with open(self.file, 'w') as fd:
            header = ['Data', 'Hora', data_name]
            writer = csv.writer(fd)
            writer.writerow(header)

logger = Logger('error_log.csv')
user_logger = Logger('log.csv')