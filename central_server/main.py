from math import floor
from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread, Lock
from typing import Dict, List, Tuple
import json
import time
import curses
import sys

from message_queue import MessageQueue
from context import Floor
from logger import logger, user_logger

current_floor = None
floor_map: List[Floor] = list()
stdscr: 'curses._CursesWindow' = None
lock = Lock()

alarme_lock = Lock()
alarme_incendio = 0
help_screen: bool = False
cursor = 1
cursor_pin = -1

hall_last_time = None

def update_alarme_incendio(inc_dec: str):
    global alarme_incendio, alarme_lock
    alarme_lock.acquire()
    if inc_dec == 'inc':
        if alarme_incendio < len(floor_map):
            alarme_incendio += 1
    elif inc_dec == 'dec':
        if alarme_incendio > 0:
            alarme_incendio -= 1

    alarme_lock.release()


def register_floor(floor: Floor, win: 'curses._CursesWindow'):
    floor_map.append(floor)
    floor.set_win(win)
    # if current_floor is None:
        # change_current_floor(0)

def change_current_floor(new):
    global current_floor, cursor_pin
    lock.acquire()
    current_floor = new
    cursor_pin = -1
    lock.release()
    # if current_floor is None:
        # write_if_no_connections()
    # else:
        # floor_map[current_floor].print_data()

def draw_general_info():
    global stdscr, alarme_incendio
    counter_people_in_building = 0
    for f in floor_map:
        counter_people_in_building += f.counter

    alarme_incendio_status = "ON" if alarme_incendio == 1 else "OFF"
    stdscr.addstr(0, 1, f"Número de pessoas no predio: {counter_people_in_building}")
    stdscr.addstr(1, 1, f"Alarme de Incendio: {alarme_incendio_status}")

    stdscr.addstr(20, 1, f"Aperte a tecla h para exibir os controles")
    
    if current_floor is not None:
        if current_floor > 0:
            stdscr.addstr(18, 1, '<-')
        elif current_floor < len(floor_map)-1:
            stdscr.addstr(18, 6, '->')

def draw(y_start: int, x_start: int, data: List[Tuple[str, str, int]]):
    global stdscr, cursor, cursor_pin

    if cursor >= len(data):
        cursor = len(data)

    draw_general_info()
    y = y_start
    for i, d in enumerate(data):
        t, _type, pin = d
        if cursor == i:
            if _type == 'in':
                stdscr.addch(y, 0, '-')
            if _type == 'out':
                stdscr.addch(y, 1, '>')
                cursor_pin = pin
        stdscr.addstr(y, x_start, t)
        y += 1

def draw_help():
    global stdscr
    stdscr.addstr(1, 1, "h: entra e sai da tela de comandos")
    stdscr.addstr(2, 1, "q: sai da aplicacao")
    stdscr.addstr(3, 1, "direcionais para dos lados: troca entre andares")
    stdscr.addstr(4, 1, "direcionais para cima e baixo: move o cursor*")
    stdscr.addstr(5, 1, "Enter: liga/desliga aparelho selecionado")
    stdscr.addstr(6, 1, "(só funciona onde aparece o cursor '>')")
    stdscr.addstr(7, 1, "l/d: desliga/liga todo o predio")
    stdscr.addstr(8, 1, "i/o: desliga/liga todo o andar atualmente selecionado")
    stdscr.addstr(10, 1, "* O cursor é um dos caracteres '>' ou '-'")
    stdscr.addstr(11, 1, "'>' significa um dispositivo de saida e pode ser ativado/desativado")
    stdscr.addstr(12, 1, "'-' significa um dispositivo de entrada, e não tem interação")

def hall_bulb_turn(on_off: int):
    global floor_map
    for f in floor_map:
        for dev in f.outDevices.values():
            if dev.type_ == "lampada" and "corredor" in dev.tag.lower():
                msg = {
                    "pin": dev.pin,
                    "data": on_off
                }
                f.mq.enqueue_message(json.dumps(msg))

def hall_timeout():
    global hall_last_time
    if hall_last_time is None:
        return
    if int(time.time() - hall_last_time) >= 15:
        hall_bulb_turn(0)
        hall_last_time = None

def set_hall_counter():
    global hall_last_time
    hall_last_time = time.time()

def refresh():
    global stdscr
    stdscr.clear()
    if help_screen:
        draw_help()
        return
    if current_floor is None:
        write_if_no_connections(stdscr)
    else:
        data = floor_map[current_floor].print_data()
        draw(3, 2, data)

def register_floor_to_stdscr(floor: Floor):
    global stdscr
    register_floor(floor, stdscr)

def write_if_no_connections(win: 'curses._CursesWindow'):
    win.addstr(0, 0, "Sem conexoes")

def open_socket(config_file: str) -> socket:
    with open(config_file, "r") as fd:
        config = json.loads(fd.read())
    
    host: str = config.get('ip_servidor_central')
    port: str = config.get('porta_servidor_central')
    
    sock = socket(AF_INET, SOCK_STREAM)
    sock.bind((host, port))

    sock.listen()
    return sock

def start_curses() -> 'curses._CursesWindow':
    stdscr: 'curses._CursesWindow' = curses.initscr()
    curses.noecho()
    curses.cbreak()
    stdscr.keypad(1)
    stdscr.nodelay(True)
    return stdscr

def close_curses(stdscr: 'curses._CursesWindow'):
    stdscr.nodelay(False)
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()

def register_devices(floor: Floor, data) -> Floor:
    for dev in data.get("data"):
        floor.register_device(dev)

def set_state(floor: Floor, pin: int, io: str, state: int):
    floor.set_state(pin, io, state)

def handle_msg(floor: Floor, data: Dict):
    global alarme_incendio, user_logger

    _type = data.get('type')
    if _type == 'counter':
        floor.counter = data.get('data')
    elif _type == 'dht22':
        floor.temperature = data.get('temperatura')
        floor.humidity = data.get('umidade')
    elif _type == 'lampada':
        set_state(floor, data.get('pin'), 'o', data.get('data'))
    elif _type == 'ar-condicionado':
        set_state(floor, data.get('pin'), 'o', data.get('data'))
    elif _type == 'aspersor':
        set_state(floor, data.get('pin'), 'o', data.get('data'))
    elif _type == 'presenca':
        set_state(floor, data.get('pin'), 'i', data.get('data'))
        if data.get('data') == 1:
            hall_bulb_turn(1)
            set_hall_counter()
    elif _type == 'fumaca':
        set_state(floor, data.get('pin'), 'i', data.get('data'))
        fire_state = data.get('data')
        if fire_state == 1:
            update_alarme_incendio('inc')
            turn_sprinklers(1)
            user_logger.log('system: Fumaça detectada. Ligar alarme de incêndio e aspersores')
        else:
            update_alarme_incendio('dec')
            if alarme_incendio == 0:
                turn_sprinklers(0)
                user_logger.log('system: Desligar alarme de incêndio e aspersores')
    elif _type == 'janela':
        set_state(floor, data.get('pin'), 'i', data.get('data'))
    elif _type == 'porta':
        set_state(floor, data.get('pin'), 'i', data.get('data'))
    
    # refresh(floor)

def handleRead(floor: Floor, *args):
    try:
        while True:
            data: bytes = floor.mq.conn.recv(2048)
            if not data:
                break
            data_ = json.loads(data.decode("utf-8"))
            if data_.get("name"):
                floor.set_name(data_.get("name"))
                register_devices(floor, data_)
                continue
            if not data_.get("type"):
                # print(data_)
                continue
            handle_msg(floor, data_)
    except Exception as e:
        logger.log(str(e))
        return

def handleWrite(floor: Floor, *args):
    # with floor.mq.conn
    try:
        while True:
            msg = floor.mq.pop_message()
            if msg:
                floor.mq.send_msg(msg)
            time.sleep(0.05)
    except Exception as e:
        logger.log(str(e))
        return

def handleConnections(sock: socket, *args):
    while True:
        conn, addr = sock.accept()
        mq = MessageQueue(conn)
        floor = Floor(mq)
        register_floor_to_stdscr(floor)
        read_thread = Thread(None, handleRead, args=(floor, addr))
        write_thread = Thread(None, handleWrite, args=(floor, addr))
        read_thread.start()
        write_thread.start()

def go_down():
    global cursor
    if cursor < 15:
        cursor += 1

def go_up():
    global cursor
    if cursor > 1:
        cursor -= 1

def send_control():
    global floor_map, user_logger
    if cursor_pin < 0:
        return
    msg = {
        'pin': cursor_pin,
        'data': int(not floor_map[current_floor].outDevices[cursor_pin].state)
    }
    tag = floor_map[current_floor].outDevices[cursor_pin].tag
    st = 'Ligar' if msg['data'] == 1 else 'Desligar'
    user_logger.log(f'{st} {tag}')
    floor_map[current_floor].mq.enqueue_message(json.dumps(msg))

def turn_all(on_off: int):
    global floor_map
    for f in floor_map:
        for dev in f.outDevices.values():
            if dev.type_ == "aspersor":
                continue
            msg = {
                "pin": dev.pin,
                "data": on_off
            }
            f.mq.enqueue_message(json.dumps(msg))

def turn_current_all(on_off: int):
    global floor_map
    for dev in floor_map[current_floor].outDevices.values():
        if dev.type_ == "aspersor":
            continue
        msg = {
            "pin": dev.pin,
            "data": on_off
        }
        floor_map[current_floor].mq.enqueue_message(json.dumps(msg))

def turn_sprinklers(on_off):
    global floor_map
    for f in floor_map:
        for dev in f.outDevices.values():
            if dev.type_ == "aspersor":
                msg = {
                    "pin": dev.pin,
                    "data": on_off
                }
                f.mq.enqueue_message(json.dumps(msg))

def get_current_floor_name():
    global floor_map, current_floor
    return floor_map[current_floor].name

def user_loop():
    global current_floor, floor_map, help_screen, user_logger, logger
    logger.log_header('Erro')
    user_logger.log_header('Acao')
    user_logger.log('Servidor começou a executar')
    while True:
        refresh()
        time.sleep(0.1)
        hall_timeout()
        if current_floor is None and len(floor_map) > 0:
            change_current_floor(0)
        c = stdscr.getch()
        if c == -1:
            continue
        if chr(c) in 'qQ':
            user_logger.log('Servidor encerrado')
            close_curses(stdscr)
            sys.exit(0)
        elif chr(c) in 'hH':
            # help :)
            help_screen = not help_screen
            continue
        if current_floor is None:
            continue
        if c == curses.KEY_LEFT:
            if current_floor > 0:
                change_current_floor(current_floor - 1)
        elif c == curses.KEY_RIGHT:
            if current_floor < len(floor_map)-1:
                change_current_floor(current_floor + 1)
        elif c == curses.KEY_DOWN:
            go_down()
        elif c == curses.KEY_UP:
            go_up()
        elif c == curses.KEY_ENTER or chr(c) == '\n':
            send_control()
        elif chr(c) in 'dD':
            user_logger.log('Desligar todos os dispositivos do prédio')
            turn_all(0)
        elif chr(c) in 'lL':
            user_logger.log('Ligar todos os dispositivos do prédio')
            turn_all(1)
        elif chr(c) in 'Ii':
            user_logger.log(f'Desligar todos os dispositivos do {get_current_floor_name()}')
            turn_current_all(0)
        elif chr(c) in 'oO':
            user_logger.log(f'Ligar todos os dispositivos do {get_current_floor_name()}')
            turn_current_all(1)
            

def main():
    try:
        global stdscr 
        stdscr = start_curses()
        write_if_no_connections(stdscr)

        sock = open_socket("server_config.json")
        connThread = Thread(None, handleConnections, args=(sock,))
        connThread.start()
        
        user_loop()
    except Exception as e:
        close_curses(stdscr)
        logger.log(str(e))

if __name__ == "__main__":
    main()