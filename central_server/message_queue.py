from socket import socket
from threading import Lock
from queue import Queue, Empty

from typing import Optional
from logger import logger

class MessageQueue:

    lock: Lock
    queue: Queue
    conn: socket

    def __init__(self, conn):
        self.lock = Lock()
        self.queue = Queue()
        self.conn = conn

    def enqueue_message(self, msg: str):
        # self.lock.acquire()
        self.queue.put(msg)
        # self.lock.release()

    def pop_message(self) -> Optional[str]:
        try:
            # self.lock.acquire()
            msg = self.queue.get()
            self.queue.task_done()
            # self.lock.release()
            return msg
        except Empty:
            return None

    def send_msg(self, msg):
        self.conn.sendall(bytes(msg, 'ascii'))
    